
Ext.define('MyApp.view.main.Form', {
    extend: 'Ext.form.Panel',
    alias: 'widget.form',
    xtype: 'form',
    defaultType: 'textfield',
    viewModel: { type: 'FormViewModel' },
    controller: 'main',
   
    items: [
        {
            xtype: 'form',
            title: 'Form',
            height: '350',
            items: [{
                xtype: 'numberfield',

                name: 'id',
                label: 'User Id',
                allowBlank: false,
                minLength: '2'
            },
            {
                xtype: 'textfield',
                label: ' Name',
                name: 'name',
                allowBlank: false,
                minLength: '3'

            },
            {
                xtype: 'textfield',
                label: ' Username',
                name: 'username',
                allowBlank: false,
                minLength: '3'

            },
            {
                xtype: 'textfield',
                label: 'Email',
                name: 'email',
                allowBlank: false,
                minLength: '3'

            },
            {
                xtype: 'numberfield',
                label: 'Phone',
                name: 'phone',
            },

            ],

            buttons: [
                {
                    text: 'submit',
                    handler: 'onClick'
                },
                {
                    text : 'Reset',
                    handler: 'clearForm'
                }
            ]
        },

        {
            xtype: 'grid',
            width: '100%',
            height: '50%',
            title: ' Grid',
            bind: {
                store: '{Form}'
            },

            columns: [{
                text: 'Id',
                dataIndex: 'id',
                width: 100,
            }, {
                text: 'Name',
                dataIndex: 'name',
                width: 100,
                cell: {
                    userCls: 'bold'
                }
            }, {
                text: 'Username',
                dataIndex: 'username',
                width: 100,
                cell: {
                    userCls: 'bold'
                }
            }, {
                text: 'Email',
                dataIndex: 'email',
                width: 100,
                cell: {
                    userCls: 'bold'
                }
            },
        {
            text: 'Phone',
            dataIndex: 'phone',
            width:100,
            cell: {
                userCls: 'bold'
            }
        }]
        },
    ]
});

