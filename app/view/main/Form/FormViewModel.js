Ext.define('MyApp2.view.main.FormViewModel', {
    extend: 'Ext.app.ViewModel',
    alias:'viewmodel.FormViewModel',
    stores:{
        Form: {
            
            model: 'Form',
            autoLoad: true,
            data: { items: []},
           
            proxy: {
                type: 'ajax',
                url: 'https://jsonplaceholder.typicode.com/users',
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            }
        }
    }
    });
    