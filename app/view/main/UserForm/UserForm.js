Ext.define('MyApp2.view.main.UserForm',{
    extend:'Ext.form.Panel',
    
    //defaultType: 'textfield',
   xtype: 'userform',
   // height: 800,
    //width: 600,
    bodyPadding: 20,
    //viewModel: { type: 'UserViewModel' },
    controller: 'main',
   
        //   xtype: 'form',
          title: 'Form',
          items: [
             {
                xtype: 'numberfield',
                label: 'Id',
                name: 'id'
             },
          {
              xtype: 'textfield',
              label: 'First Name',
              name: 'firstName',
              allowBlank: false,
              minLength: '3',
              

          },
          {
            xtype: 'textfield',
            label: 'Middle Name',
            name: 'middleName'
         },
          {
              xtype: 'textfield',
              label: 'Last Name',
              name: 'lastName',
              allowBlank: false,
              minLength: '3'

          },
          {
            xtype: 'numberfield',
            label: 'Phone no: ',
            name: 'phone',
            
        },{
           xtype: 'textfield',
           label: 'City',
           name:'city'
        },{
           xtype: 'textfield',
           label: 'State',
           name: 'state'
        },
      ],

          buttons: [
              {
                  text: 'submit',
                  //formBind: true,
                  handler: 'onSubmit'
              },
              {
                  text : 'Reset',
                  handler: 'clearForm'
              },
              {
                 text:'Fill Details',
                 handler: 'onReadClick'
              }
          ]
    
        
});
    
   
    
    
   
