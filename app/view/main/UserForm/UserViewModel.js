Ext.define('MyApp2.view.main.UserViewModel', {
    extend: 'Ext.app.ViewModel',
    alias:'viewmodel.UserViewModel',
    stores: {
        Personnel: {
            model: 'Personnel',
            autoLoad: true,
            data: { items: [
                { id: "1", firstName: 'Jean Luc', city: "Delhi" },
                { id: "2", firstName: 'Worf', city: "Noida" },
                { id: "3", firstName: 'Deanna', city: "Gurgaon" },
                { id: "4", firstName: 'Data', city: "Jaipur" }
               ]},

               proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'items'
                }
            }
        },

    User: {
        //model: 'User',
        storeId: 'userstore',
        autoLoad: true,
        data: { items: [
            { id: "1", firstName: 'Jean Luc',middleName:"A", lastName: "abc", phone: "555-111-1111", city: "Delhi",state: "Delhi" },
            { id: "2", firstName: 'Worf', middleName: "B", lastName: "xyz",  phone: "555-222-2222", city: "Noida", state:"UP" },
            { id: "3", firstName: 'Deanna',middleName: "C", lastName: "jkl", phone: "555-333-3333", city: "Jaipur", state:"Rajasthan" },
            { id: "4", firstName: 'Data',middleName:"D", lastName: "xyz",phone: "555-444-4444", city: "Gurgaon", state: "Haryana" },
            { id: "5", firstName: 'Jean Luc',middleName:"A", lastName: "abc", phone: "555-111-1111", city: "Delhi",state: "Delhi" },
            { id: "6", firstName: 'Worf', middleName: "B", lastName: "xyz",  phone: "555-222-2222", city: "Noida", state:"UP" },
            { id: "7", firstName: 'Deanna',middleName: "C", lastName: "jkl", phone: "555-333-3333", city: "Jaipur", state:"Rajasthan" },
            { id: "8", firstName: 'Data',middleName:"D", lastName: "xyz",phone: "555-444-4444", city: "Gurgaon", state: "Haryana" },
            { id: "9", firstName: 'Deanna',middleName: "C", lastName: "jkl", phone: "555-333-3333", city: "Jaipur", state:"Rajasthan" }
        
        ]},
    
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
},

UserChain : {
    source: 'userstore',
    sorters: [{
        property: 'firstName',
        direction: 'desc'
    }]
}
}

});
