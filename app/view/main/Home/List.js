
Ext.define('MyApp2.view.main.List', {
    extend: 'Ext.panel.Panel',
    xtype: 'mainlist',
    width: '100%',
    height: '100%',
    itemId:'mainlist',
   
   requires: [
        //'MyApp2.store.Personnel',
        //'MyApp.store.User',
        //'MyApp.view.main.MainController',
    ],
    controller: 'main',
    //viewModel: { type: 'UserViewModel' },

    items: [
        {
            xtype: 'grid',
            width: '100%',
            height: '50%',
            title: 'Parent Grid',
            bind: {
                store: '{Personnel}'
            },
            listeners: {
                select: 'onItemSelected'
                },
        
            columns: [{ 
                text: 'Id',
                dataIndex: 'id',
                width: 100,
            },{
                text: 'FirstName',
                dataIndex: 'firstName',
                width: 100,
                cell: {
                    userCls: 'bold'
                }
            },{
                text: 'City',
                dataIndex: 'city',
                width: 100,
                cell: {
                    userCls: 'bold'
                }
            }]
        },
{
    xtype: 'grid',
    title: 'Child Grid',
    itemId:'childGrid',
    width: '100%',
    height: '50%',
    bind: {
        store: '{User}'
    },
    

    columns: [
    { 
        text: 'Id',
        dataIndex: 'id',
        width: 100,
    },{
        text: 'FirstName',
        dataIndex: 'firstName',
        width: 100,
        cell: {
            userCls: 'bold'
        }
    },{
        text: 'MiddleName',
        dataIndex: 'middleName',
        width: 100,
        cell: {
            userCls: 'bold'
        }
    },{
        text: 'LastName',
        dataIndex: 'lastName',
        width: 100,
        cell: {
            userCls: 'bold'
        }
    },{
        text: 'Phone',
        dataIndex: 'phone',
        width: 150 
    },{
        text: 'City',
        dataIndex: 'city',
        width: 100,
        cell: {
        userCls: 'bold'
            },
        
    },{
        text: 'State',
        dataIndex: 'state',
        width: 100,
        cell: {
        userCls: 'bold'
        },
    
    
    },
]
}
],


}
);
