/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('MyApp2.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        //Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
        console.log(record);
        var myValue = record[0].get('city');
        console.log(myValue);
        var store = this.getViewModel().getStore('User');
        //var store = this.getView().items.items[1].getStore();
        store.filter('city', myValue);
    },

    onReadClick: function (sender, record) {
        var store = this.getViewModel().getStore('User');
        console.log(store);
        var data = store.data.items[0].data;
        this.getView('user').setValues(data);
    },

    clearForm: function () {
        this.getView().reset();
    },


    onSubmit: function (sender, record, ) {

        var data = this.getView().getValues();
        console.log(data);
        var store = this.getViewModel().getStore('User');
        //var store= this.getView().down('userform').getViewModel().getStore('User');
        console.log(store);
        store.add(data);

    },

    onClick: function (sender, record) {
        var store = this.getViewModel().getStore('Form');
        var formData = this.getView().getValues();
        //var formData= this.getView().up('form').getValues();
        Ext.Ajax.request({
            url: 'https://jsonplaceholder.typicode.com/users',
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            params: Ext.JSON.encode(formData),
            success: function (response) {
                var response = Ext.decode(response.responseText);
                store.add(response);
                store.loadData(response);
                Ext.Msg.alert('Status', 'Saved successfully.');
                console.log(store);

            },
        });


    }



});
