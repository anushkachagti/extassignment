
Ext.define('MyApp2.view.main.Test', {
    extend: 'Ext.panel.Panel',
    xtype: 'test',
    width: '100%',
    height: '100%',
   
   
   requires: [
       
    ],
    
    controller: 'main',
    items: [
        {
            xtype: 'grid',
            width: '100%',
            height: '50%',
            title: 'User Chained Store',
            bind: {
                store: '{UserChain}'
            },
            listeners: {
                select: 'onItemSelected'
                },
        
            columns: [{ 
                text: 'Id',
                dataIndex: 'id',
                width: 100,
            },{
                text: 'FirstName',
                dataIndex: 'firstName',
                width: 100,
                cell: {
                    userCls: 'bold'
                }
            },{
                text: 'City',
                dataIndex: 'city',
                width: 100,
                cell: {
                    userCls: 'bold'
                },
               
            }]
        },
        
    ],


}
);