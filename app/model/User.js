Ext.define('MyApp2.model.User', {
    extend: 'MyApp2.model.Base',

    fields: [
        { name: 'id', type: 'int' },
        { name: 'firstName', type: 'string' },
        { name: 'middleName', type: 'string' },
        { name: 'lastName', type: 'string' },
        { name: 'phone', type: 'string' },
        { name: 'city', type: 'string' },
        { name: 'state', type: 'string' }
    ]

    
    
});
