Ext.define('MyApp2.model.UserForm', {
    extend: 'MyApp2.model.Base',

    fields: ['id','firstName', 'middleName','lastName', 'phone','city','state']

});
