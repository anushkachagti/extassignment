Ext.define('MyApp2.model.Form', {
    extend: 'MyApp2.model.Base',

    fields: ['id','name', 'username', 'email', 'phone']

});
