Ext.define('MyApp2.model.Personnel', {
    extend: 'MyApp2.model.Base',

    fields: [
        { name: 'id', type: 'int' },
        { name: 'firstName', type: 'string' },
        { name: 'city', type: 'string' }
    ],
});
